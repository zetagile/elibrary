package com.myorg.elibrary.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myorg.elibrary.entity.BookEntity;
import com.myorg.elibrary.entity.IssuebookEntity;
import com.myorg.elibrary.entity.MemberEntity;
import com.myorg.elibrary.service.MemberBookService;
import com.myorg.elibrary.service.MemberIssueBookService;

@Controller
public class MemberIssueBookContoller {
@Autowired
MemberIssueBookService memberIssueBookService;
@Autowired
MemberBookService memberbookService;
@Autowired
HttpSession session;
	@RequestMapping(value="/requestBook/{bookId}")
	public String addIssueBookCart(@PathVariable(value="bookId")String bookId,Model m ) {
		
		
	String add=null;
	BookEntity book= memberbookService.getBookById(bookId);
	MemberEntity member=(MemberEntity) session.getAttribute("member");
	//Integer studentId = (Integer) session.getAttribute("memberId");
	//System.out.println(member.getUsername());System.out.println(book.getAuthor());
	try {
	if((book.getQuantity()!=0 ||book.getIssued()==0) && (book.getQuantity()>book.getIssued()))
	{
		 add=memberIssueBookService.addUserBookToCartIssue(member,bookId);
	}
	
	if(add.equals("success"))
	{ m.addAttribute("err", "Book Added Successfully");
		return "cart";
		
	}	
	else {
		 m.addAttribute("err", "Failed to add book.Book has been already checked out or waiting ");
		 return "cart";
	}}
		
catch(NullPointerException e){
		 m.addAttribute("err", "Failed to add book.Book has been already checked out or waiting");
		 return "cart";
	}}
	
	@RequestMapping(value = "/issueBooks", method = RequestMethod.GET)
	public String viewIssueBooksInCart(ModelMap map) {
		 Integer studentId = (Integer) session.getAttribute("memberId");
		 String bookStatus="Books in Cart";
			map.addAttribute("bookStatus", bookStatus);
		map.addAttribute("E_ISSUEBOOK", new IssuebookEntity());
		map.addAttribute("IssuebookList", memberIssueBookService.getUserBooksInCartIssue(studentId));
		System.out.println(studentId);
		return "issueBooks";
	
	}
	
	@RequestMapping("/delete/{callNo}")
	public String removeBookFromCart(@PathVariable("callNo") String callNo) {
		Integer studentId = (Integer) session.getAttribute("memberId");
		System.out.println(callNo);
		memberIssueBookService.removeCartEntry(callNo,studentId);;
		return "redirect:/issueBooks";
	}
	@RequestMapping(value = "/issuedBooks", method = RequestMethod.GET)
	public String issueBooks(ModelMap map) {
		 Integer studentId = (Integer) session.getAttribute("memberId");
			String bookStatus="Total Books  ";
			map.addAttribute("bookStatus", bookStatus);
		map.addAttribute("E_ISSUEBOOK", new IssuebookEntity());
		map.addAttribute("issuedBooks", memberIssueBookService.getBookByStudentId(studentId));

		return "issuedBooks";
	}
	@RequestMapping(value = "/overDue", method = RequestMethod.GET)
	public String overDueBooks(ModelMap map) {
		String bookStatus="OverDue Books  ";
		map.addAttribute("bookStatus", bookStatus);
		Integer studentId = (Integer) session.getAttribute("memberId");
		map.addAttribute("E_ISSUEBOOK", new IssuebookEntity());
		map.addAttribute("issuedBooks", memberIssueBookService.overDueBooks(studentId));

		return "issuedBooks";
	}
	@RequestMapping(value = "checkout",method= RequestMethod.GET)
     
	public String checkOut(ModelMap map) {
		 Integer studentId = (Integer) session.getAttribute("memberId");
		 
		 List<IssuebookEntity>iBS=memberIssueBookService.getUserBooksInCartIssue(studentId);
			for(IssuebookEntity i:iBS)
			{
				 memberIssueBookService.updaterequestBookStatus(studentId,i.getCallNo());
			}
		
        return "checkout"; //JSP
    }
	@RequestMapping(value = "/pendingBooks", method = RequestMethod.GET)
	public String requestPendingBooks(ModelMap map) {
		String bookStatus="Books requested from librarian  ";
		map.addAttribute("bookStatus", bookStatus);
		Integer studentId = (Integer) session.getAttribute("memberId");
		map.addAttribute("E_ISSUEBOOK", new IssuebookEntity());
		map.addAttribute("issuedBooks", memberIssueBookService.pendingBooks(studentId));

		return "issuedBooks";
	}
	
	
////	@RequestMapping(value = "/waitlistBooks", method = RequestMethod.GET)
////	public String getBookByStudentId(ModelMap map) {
////		
////		map.addAttribute("BookList", memberIssueBookService.waitlistBooks(callNo, studentId););
////
////		return "Message";
////	}

}
