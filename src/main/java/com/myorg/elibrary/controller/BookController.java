
package com.myorg.elibrary.controller;
  
import org.springframework.beans.factory.annotation.Autowired;


import  org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import  org.springframework.web.bind.annotation.RequestMapping;
import  org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.myorg.elibrary.entity.BookEntity;
import com.myorg.elibrary.service.MemberBookService;

  @Controller
  public class BookController {
  
  @Autowired 
  private MemberBookService memberBookService;
  
  @RequestMapping(value = "/viewbooks", method = RequestMethod.GET) 
  public String listofBooks(ModelMap map) { 
	  map.addAttribute("e_book", new BookEntity()); 
	  map.addAttribute("bookList", memberBookService.viewAllBooks());
  
  return "bookListView"; 
  }
  
  @RequestMapping(value = "/viewbooksByName", method = RequestMethod.GET) 
  public String listofBooksName(ModelMap map) { 
	  map.addAttribute("e_book", new BookEntity()); 
	  map.addAttribute("bookList", memberBookService.viewAllBooks());
  
  return "bookListByName"; 
  }
  
  @RequestMapping(value = "/viewbooksByAuthor", method = RequestMethod.GET) 
  public String listofBooksAuthor(ModelMap map) { 
	  map.addAttribute("e_book", new BookEntity()); 
	  map.addAttribute("bookList", memberBookService.viewAllBooks());
  
  return "bookListByAuthor"; 
  
   }
  
	
	
	  @RequestMapping(value = "/searchingAuthor", method = RequestMethod.GET)
	  public String searchlistofBooks(@RequestParam("author") String author, ModelMap map) {
	  
	  map.addAttribute("e_book", new BookEntity()); 
	  map.addAttribute("bookList", memberBookService.getBookByAuthor(author));
	  
	  return "bookListView"; 
	  }
	 
	  @RequestMapping(value = "/searchingName", method = RequestMethod.GET)
	  public String searchlistofBooksName(@ModelAttribute(value = "name") String name, ModelMap map) {
	  
	  map.addAttribute("e_book", new BookEntity()); 
	  map.addAttribute("bookList", memberBookService.getBookByName(name));
	  
	  return "bookListView"; 
	  }
	  
	  
	   
	    
	    
	}
	    
	    
	    
	   
	  
	  
	 
 