package com.myorg.elibrary.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myorg.elibrary.entity.BookEntity;
import com.myorg.elibrary.entity.IssuebookEntity;
import com.myorg.elibrary.service.LibrarianService;
//import com.springmvctraining.hqlexample.entity.BookEntity;


/**
 * Handles requests for the application home page.
 */
@Controller
public class LibrarianController {

	@Autowired
	private LibrarianService librarianService;
	
	//private static final Logger logger = LoggerFactory.getLogger(LibrarianController.class);
	
	@RequestMapping(value = "/addbook", method = RequestMethod.GET)
	public String addbook(ModelMap map) {
		map.addAttribute("book", new BookEntity());
	
		return "AddBook";
	}
	
	@RequestMapping(value = "/HomeLib", method = RequestMethod.GET)
	public String home(ModelMap map) {
		//map.addAttribute("book", new BookEntity());
	
		return "librarianHomePage";
	}
	

	@RequestMapping(value = "/viewAvailableBook", method = RequestMethod.GET)
	public String listBook(ModelMap map1) {
		map1.addAttribute("book", new BookEntity());
		map1.addAttribute("bookList", librarianService.getAllBook());

		return "viewAvailableBook";
	}
	
	
	@RequestMapping(value = "/viewIssuedBook", method = RequestMethod.GET)
	public String listIssuedBook(ModelMap map1) {
		map1.addAttribute("book", new IssuebookEntity());
		map1.addAttribute("issuedBookList", librarianService.getAllIssuedBook());

		return "Issuedbook";
	}	
	
	@RequestMapping(value = "/issueBook", method = RequestMethod.GET)
	public String listIssuedBook1(ModelMap map2) {
		map2.addAttribute("book", new IssuebookEntity());
		map2.addAttribute("issuedBookList1", librarianService.getPendingApprovalBook());
		
		return "pendingApprovalIssueBook";
	}

	//controller mapping for approve request by librariran for issue book
	@RequestMapping(value = "/approve/{callno}/{studentID}")
		public String updateIssuedBookApproval(@PathVariable("callno") String callno,@PathVariable("studentID") Integer student_Id,ModelMap map) {
		
		librarianService.updatestatus(callno,student_Id);
		map.addAttribute("issuedBookList1", librarianService.getPendingApprovalBook());
		return "pendingApprovalIssueBook";
	}
	
	@RequestMapping(value = "/addbookdetails", method = RequestMethod.POST)
	public String addBook(@ModelAttribute(value = "book") BookEntity book, BindingResult result) {
		librarianService.addbook(book);
		return "redirect:/HomeLib";
	}
	
	
	

}
