package com.myorg.elibrary.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import com.myorg.elibrary.service.AdminService;
import com.myorg.elibrary.entity.LibrarianEntity;

@Controller
public class AdminController {

	@Autowired
	@Qualifier("adminService")
	private AdminService adminService;
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public String listLibraians(ModelMap model, HttpServletRequest request) {
		List<LibrarianEntity> thelibrarians = adminService.listLibrarians();
		model.addAttribute("librarianEntity",thelibrarians);
		return "viewLibrarian";
		
	}
	@RequestMapping(value="/showForm", method=RequestMethod.GET)
	public String showFormAdd(ModelMap model, HttpServletRequest request) {
		
		LibrarianEntity librarianEntity = new LibrarianEntity();
		model.addAttribute("librarianEntity",librarianEntity );
		return "addLibrarian";
		
	}

	@RequestMapping(value="/saveLibrarian", method=RequestMethod.POST)
	public String saveLibrarian(@ModelAttribute("librarianEntity")LibrarianEntity librarianEntity,Model model,HttpServletRequest request) {
		
		adminService.saveLibrarian(librarianEntity);
		return "redirect:/showForm";
	}
		
	@RequestMapping(value="/deleteLibrarian", method=RequestMethod.GET)
	public String deleteLibrarian(@RequestParam("id") int id,Model model,HttpServletRequest request) {
		adminService.deleteLibrarian(id);
		List<LibrarianEntity> thelibrarians = adminService.listLibrarians();
		model.addAttribute("librarianEntity",thelibrarians);
		return "viewLibrarian";
	}
	
	@RequestMapping(value="/editLibrarian", method=RequestMethod.GET)
	public String editLibrarian(@RequestParam("id")int id, Model model,HttpServletRequest request) {
		 System.out.println("id passed="+id);
		 LibrarianEntity librarianEntity = adminService.getLibrarianById(id);
		 model.addAttribute("librarianEntity",librarianEntity);
		 System.out.println(librarianEntity.getId());
		 return "addLibrarian";
	}
	
}
