/*package com.myorg.elibrary.controller;
  
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import  org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import  org.springframework.web.bind.annotation.RequestMapping;
import  org.springframework.web.bind.annotation.RequestMethod;
import com.myorg.elibrary.entity.LoginEntity;
import com.myorg.elibrary.service.LoginService;

  @Controller
  public class LoginController {
  
	  @Autowired private LoginService userService;
	  
	  @RequestMapping(value = "/", method= RequestMethod.GET)
		public String loginPage(ModelMap model){
			
			System.out.println("I am in login page");
			model.addAttribute("user1", new LoginEntity());
			return "elibrary-Login";
	}
  
  @RequestMapping(value = "/login", method = RequestMethod.POST)
	public String memberlogin(@ModelAttribute("user1") LoginEntity user1, Model model, HttpServletRequest  request) throws UnsupportedEncodingException {
		
//	  		  request.setCharacterEncoding("UTF-8");
	  		  model.addAttribute("uname", request.getParameter(user1.getUname()));
	  		  model.addAttribute("pwd", request.getParameter(user1.getPwd()));
 		      model.addAttribute("userprofile",request.getParameter(user1.getUserprofile()));
		      String uprofile = user1.getUserprofile();
		      Integer res = userService.checkUser(user1);
		      if(res==0)
		    	  {
		    	  //JOptionPane.showMessageDialog(null,"Invalid credentials");
		    	  model.addAttribute("error", "You are not a valid user");
					return "elibrary-Login";
		    	  }
		      else if(res==1) {
		    	  System.out.println("userprofile = "+ uprofile);
//		    	  
		    	  return "redirect:/showForm";
		    		
		      }
//		      else if(res==2)
//		    	  		 return "memberView"; 
		    	   else if(res==3)
		    	   			 return "memberView";
		    	   		else {
		    	   			System.out.println("userprofile = "+ uprofile);
		    	   			model.addAttribute("error", "Choose a correct user profile");
							return "elibrary-Login";
		    	   		}
		    	   			
	   
  	}
  	  
	 	 
  }*/

package com.myorg.elibrary.controller;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myorg.elibrary.entity.LibrarianEntity;
import com.myorg.elibrary.entity.LoginEntity;
import com.myorg.elibrary.entity.MemberEntity;
import com.myorg.elibrary.service.LoginService;

@Controller
public class LoginController {

	@Autowired
	private LoginService userService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String loginPage(ModelMap model) {

		System.out.println("Entered login page");
		model.addAttribute("user1", new LoginEntity());
		return "elibrary-Login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String memberlogin(@ModelAttribute("user1") LoginEntity user1, Model model, HttpSession session,
			HttpServletRequest request) throws UnsupportedEncodingException {
		
		String retVal=null;

		request.setCharacterEncoding("UTF-8");
		model.addAttribute("uname", request.getParameter(user1.getUname()));
		model.addAttribute("pwd", request.getParameter(user1.getPwd()));
		model.addAttribute("userprofile", request.getParameter(user1.getUserprofile()));
		// String uprofile = user1.getUserprofile();
		// Integer res = userService.checkUser(user1);
//		      if(res==0)
//		    	  {
//		    	  //JOptionPane.showMessageDialog(null,"Invalid credentials");
//		    	  model.addAttribute("error", "You are not a valid user");
//					return "elibrary-Login";
//		    	  }
//		      
//		      else if(res==1) {
//		    	  System.out.println("userprofile = "+ uprofile + "logged in");
////		    	  
//		    	  
//		    		
//		      }
////		      else if(res==2)
////		    	  		 return "memberView"; 
//		    	   else if(res==3)
//		    	   			 return "memberView";
//		    	   		else {
//		    	   			System.out.println("userprofile = "+ uprofile+"logged in");
//		    	   			model.addAttribute("error", "Choose a correct user profile");
//							return "elibrary-Login";
//		    	   		}

		if (user1.getUserprofile().equals("Admin")) {
			

				System.out.println("I am Admin");
				if (user1.getUname().equals("admin") && user1.getPwd().equals("admin123")) {
					return retVal="redirect:/list";
				}

			
		}

		else if (user1.getUserprofile().equals("Member")) {
			try {System.out.println(user1.getUname());System.out.println(user1.getPwd());
				MemberEntity member = userService.checkMember(user1.getUname(), user1.getPwd());
				if (member.getMemberID() != 0) {
					addUserInSession(member, session);
					return retVal="memberView";
				} else {
					// System.out.println("userprofile = "+ uprofile+"logged in");
					model.addAttribute("error", "Choose a correct user profile");
					return retVal="elibrary-Login";
				}

			} catch (Exception e) {
				System.out.print("Exception Caught");
				return retVal="elibrary-Login";
			}
		}

		else if (user1.getUserprofile().equals("Librarian")) {
			try {
				LibrarianEntity library = userService.checkLibrarian(user1.getUname(), user1.getPwd());
				if (library.getId() != 0) {
					return retVal="librarianHomePage";
				} else {
					// System.out.println("userprofile = "+ uprofile+"logged in");
					model.addAttribute("error", "Choose a correct user profile");
					return retVal="elibrary-Login";
				}
			} catch (Exception e) {
				System.out.print("Exception Caught");
				return retVal="elibrary-Login";
			}
		}

		else {
			// System.out.println("userprofile = "+ uprofile+"logged in");
			model.addAttribute("error", "Choose a correct user profile");
			return retVal="elibrary-Login";
		}
		
return retVal;
	}

	private void addUserInSession(MemberEntity member, HttpSession session) {
		session.setAttribute("member", member);
		session.setAttribute("memberId", member.getMemberID());

	}

	@RequestMapping(value = "/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}

}
