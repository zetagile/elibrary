package com.myorg.elibrary.repository;

import java.util.List;

import com.myorg.elibrary.entity.BookEntity;
import com.myorg.elibrary.entity.IssuebookEntity;

public interface LibrarianRepository {

	public	void addbook(BookEntity book);

	public List<BookEntity> getAllBook();

	public List<IssuebookEntity> getAllIssuedBook();

	public List<IssuebookEntity> getPendingApprovalBook();

	public void updatestatus(String callno, Integer student_Id);

	
}
