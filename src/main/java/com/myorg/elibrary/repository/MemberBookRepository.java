
  package com.myorg.elibrary.repository;
  
  import java.util.List;
  
  import com.myorg.elibrary.entity.BookEntity;
  
  
  
  public interface MemberBookRepository {
  
  
  public List<BookEntity> viewAllBooks();
  public List<BookEntity> getBookByName(String sText);
  public List<BookEntity> getBookByAuthor(String sText);
 // public List<BookEntity>  getBookByText(String sText);
  public BookEntity getBookById(String callNo);
  //public BookEntity requestBook(String callno);
  
  }
 