
  package com.myorg.elibrary.repository;
  
  import com.myorg.elibrary.entity.LibrarianEntity;
import com.myorg.elibrary.entity.MemberEntity;
  
  public interface LoginRepository {
    
	  public MemberEntity checkMember(String userName,String password);
	  public LibrarianEntity checkLibrarian(String userName,String password);
  
  }
 