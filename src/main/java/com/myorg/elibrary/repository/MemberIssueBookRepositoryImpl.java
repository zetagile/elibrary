package com.myorg.elibrary.repository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.myorg.elibrary.entity.IssuebookEntity;
import com.myorg.elibrary.entity.MemberEntity;



@Repository
public class MemberIssueBookRepositoryImpl implements MemberIssueBookRepository{

	
	
@Autowired
SessionFactory sessionfactory;
@Override
public String addUserBookToCartIssue(MemberEntity member,String bookId)throws NullPointerException {
	DateFormat dtf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
Query q= this.sessionfactory.getCurrentSession().createQuery("From IssuebookEntity  WHERE studentId = :studentId and callNo=:callNo ");
	 q.setParameter("callNo", bookId);
     q.setParameter("studentId", member.getMemberID());
     
     System.out.println(bookId);System.out.println(member.getMemberID());
     
    IssuebookEntity ibs=(IssuebookEntity) q.uniqueResult();
//Criterion nm =    Restrictions.eq("callNo", bookId);
//Criterion nm1 = Restrictions.eq("studentId", member.getMemberID());
//
//    IssuebookEntity ibs=(IssuebookEntity) this.sessionfactory.getCurrentSession().createCriteria(IssuebookEntity.class).add(Restrictions.and(nm, nm1)).uniqueResult();
//	
    if(null!=ibs && ibs.getCallNo().equals(bookId))
  
     {if(ibs.getReturnStatus().equals("yes"))
     {
    	 IssuebookEntity isp=new IssuebookEntity(bookId,member.getMemberID(),member.getUsername(),member.getMobileNO(),LocalDateTime.now(),"no","request");
		 this.sessionfactory.getCurrentSession().saveOrUpdate(isp);
		 
		 System.out.println("success");
			return "success";
     }
     else
    	 {System.out.println("back");
    	 return "failure";}}
     else {
		 
		 IssuebookEntity isp=new IssuebookEntity(bookId,member.getMemberID(),member.getUsername(),member.getMobileNO(),LocalDateTime.now(),"no","request");
		 this.sessionfactory.getCurrentSession().saveOrUpdate(isp);
		 
		 System.out.println("success");
			return "success";
	 }
 }


//@Override
//public String addUserBookToCartIssue(Integer memberId,String bookId)throws NullPointerException {
//	DateFormat dtf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
////Query q= this.sessionfactory.getCurrentSession().createQuery("From IssuebookEntity  WHERE studentId = :studentId and callNo=:callNo ");
////	 q.setParameter("callNo", bookId);
////     q.setParameter("studentId", memberId);
////     
////     MemberEntity member=(MemberEntity) this.sessionfactory.getCurrentSession().createQuery("From IssuebookEntity  WHERE studentId = :studentId and callNo=:callNo ").uniqueResult();
////	 
////     q.setParameter("studentId", memberId);
////     
//     //System.out.println(bookId);System.out.println(member.getMemberID());
//     
//   // IssuebookEntity ibs=(IssuebookEntity) q.uniqueResult();
//Criterion nm =    Restrictions.eq("callNo", bookId);
//Criterion nm1 = Restrictions.eq("studentId", memberId);
//Criterion nm2 = Restrictions.eq("memberID", memberId);
//    IssuebookEntity ibs=(IssuebookEntity) this.sessionfactory.getCurrentSession().createCriteria(IssuebookEntity.class).add(Restrictions.and(nm, nm1)).uniqueResult();
//	MemberEntity member=(MemberEntity) this.sessionfactory.getCurrentSession().createCriteria(MemberEntity.class).add(nm2).uniqueResult();
//	
//	System.out.println();
//    if(null!=ibs && ibs.getCallNo().equals(bookId))
//  
//     {if(ibs.getReturnStatus().equals("yes"))
//     { System.out.println(member.getUsername()+memberId+member.getMobileNO());
//    	 IssuebookEntity isp=new IssuebookEntity(bookId,memberId,member.getUsername(),member.getMobileNO(),LocalDateTime.now(),"no","request");
//		 this.sessionfactory.getCurrentSession().saveOrUpdate(isp);
//		 
//		 System.out.println("success");
//			return "success";
//     }
//     else
    	/// {System.out.println("back");
    	// return "failure";}
    // }
	
//     else if(q!=null)
//     {
   	
//    	 System.out.println(ibs);
//    	 if(ibs.getRequestBookStatus().equals("Issued")) {
//
//        	 IssuebookEntity isp=new IssuebookEntity(book.getCallNo(),member.getMemberID(),member.getUsername(),member.getMobileNO(),LocalDateTime.now(),"no","request");
//    		 sessionfactory.getCurrentSession().saveOrUpdate(isp);
//    	 }
    //	 return "success";}
    	 
//    	 else {
//    		 System.out.println(member.getUsername()+memberId+member.getMobileNO());
//    		 IssuebookEntity isp=new IssuebookEntity(bookId,memberId,member.getUsername(),member.getMobileNO(),LocalDateTime.now(),"no","request");
//    		 this.sessionfactory.getCurrentSession().saveOrUpdate(isp);
//    		 
//    		 System.out.println("success");
//    			return "success";
//    	 }
     
			
		//}
	//}


//    LocalDateTime duedate;
//	try {
//		
//		
//		duedate = dtf.parse(isp.getIssueDate());
//		
//		
//	System.out.println(duedate);
//	

		                         
//			
//	String query2="INSERT INTO IssuebookEntity(callNo,studentId,studentName,studentMobile,issueDate,returnStatus)" 
//          +" VALUES("+book.getId()+","+member.getMemberID()+","+member.getUsername()+","+member.getMobileNO()+","+LocalDateTime.now()+","+"False"+")";
//	sessionfactory.getCurrentSession().createQuery(query2);

//	catch(Exception e) {
//		System.out.println(e);






	@Override
	public void removeCartEntry(String callno,Integer studentId) {
		// TODO Auto-generated method stub
		final Query query=	this.sessionfactory.getCurrentSession().createQuery("DELETE from IssuebookEntity where callNo=:callNo AND studentId = :studentId And requestBookStatus='request'");
		query.setParameter("callNo", callno);
		 query.setParameter("studentId", studentId);
		query.executeUpdate();
	}

	@Override
	public List<IssuebookEntity> getUserBooksInCartIssue(Integer studentId) {
		// TODO Auto-generated method stub
		
		
        Query q = sessionfactory.getCurrentSession().createQuery("From IssuebookEntity  WHERE studentId = :studentId AND requestBookStatus = 'request'");
        q.setParameter("studentId", studentId);
        System.out.println(q);
       
        return q.list();
	}
	@Override
	public List<IssuebookEntity> overDueBooks(Integer studentId) {
		
		Criteria cr;
//		IssuebookEntity isp;
//		  DateFormat dtf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//	        Date duedate;
//			try {
//				duedate = dtf.parse(isp.getIssueDate());
//			
//
//	        Calendar cal = new GregorianCalendar();
//	        cal.setTime(duedate);
//	        cal.(Calendar.DATE, 30);
//
//	        String dueDate = dtf.format(cal.getTime());
//	        // System.out.println("String new due date " + dueDate);

	Calendar cal=Calendar.getInstance();
	cal.add(cal.DAY_OF_MONTH, -30);
	Date duedate=cal.getTime();
	DateFormat dtf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	 String dueDate1 = dtf.format(cal.getTime());
	System.out.println(duedate);
	        
	Criterion nm =    Restrictions.eq("studentId", studentId);
	Criterion nm1 = Restrictions.lt("issueDate", dtf.format(duedate));
return	this.sessionfactory.getCurrentSession().createCriteria(IssuebookEntity.class).add(Restrictions.and(nm,nm1)).add(Restrictions.eq("returnStatus", "no")).list();
//	Query q = sessionfactory.getCurrentSession().createQuery("From IssuebookEntity  WHERE studentId = :studentId AND issueDate >duedate");
//    q.setParameter("studentId", studentId);
//    
   // System.out.println(q);
    //return q.list();
			
}
	

	@Override
	public List<IssuebookEntity> getBookByStudentId(Integer studentId) {
		// TODO Auto-generated method stub
		Query q = sessionfactory.getCurrentSession().createQuery("From IssuebookEntity  WHERE studentId = :studentId ");
        q.setParameter("studentId", studentId);
        System.out.println(q);
       
        return q.list();
	}
//	@Override
//	public void waitlistBooks( Integer studentId,Integer callNo) {
//		// TODO Auto-generated method stub
//		Query q = sessionfactory.getCurrentSession().createQuery("Select * From E_ISSUEBOOK  WHERE studentId = :studentId ");
//        q.setParameter("studentId", studentId);
      @Override  
	public void updaterequestBookStatus(Integer studentId ,String callNo ) {
		// TODO Auto-generated method stub
		Query q = sessionfactory.getCurrentSession().createQuery("Update IssuebookEntity set requestBookStatus='pending' WHERE callNo=:callNo And studentId=:studentId ");
	
	
        q.setParameter("callNo", callNo);
        q.setParameter("studentId", studentId);
        q.executeUpdate();
        System.out.println(q);
//        Criterion nm =    Restrictions.eq("studentId", studentId);
//        Criterion nm1 =    Restrictions.eq("callNo", callNo);
//        this.sessionfactory.getCurrentSession().createCriteria(IssuebookEntity.class).
       
	}     
	
      @Override
  	public List<IssuebookEntity> pendingBooks(Integer studentId) {
  		// TODO Auto-generated method stub
  		
  		
          Query q = sessionfactory.getCurrentSession().createQuery("From IssuebookEntity  WHERE studentId = :studentId AND requestBookStatus = 'pending'");
          q.setParameter("studentId", studentId);
          System.out.println(q);
         
          return q.list();
  	}
	

}