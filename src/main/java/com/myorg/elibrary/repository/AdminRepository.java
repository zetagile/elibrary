package com.myorg.elibrary.repository;

import java.util.List;

import org.springframework.stereotype.Repository;
import com.myorg.elibrary.entity.LibrarianEntity;

@Repository
public interface AdminRepository {

	public void saveLibrarian(LibrarianEntity librarianEntity);
//	public  LibrarianEntity updateLibrarian(LibrarianEntity librarianEntity);
	public LibrarianEntity getLibrarianById(int id);
	public List<LibrarianEntity> listLibrarians();
	public void deleteLibrarian(int id);
	
}
