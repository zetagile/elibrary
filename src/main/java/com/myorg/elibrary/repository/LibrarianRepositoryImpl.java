package com.myorg.elibrary.repository;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.myorg.elibrary.entity.BookEntity;
import com.myorg.elibrary.entity.IssuebookEntity;

@Repository
public class LibrarianRepositoryImpl implements LibrarianRepository {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addbook(BookEntity book) {
		this.sessionFactory.getCurrentSession().save(book);
		
	}

	@Override
	public List<BookEntity> getAllBook() {
		return this.sessionFactory.getCurrentSession().createQuery("FROM BookEntity where QUANTITY >0").list();
	}

	@Override
	public List<IssuebookEntity> getAllIssuedBook() {
		
		return this.sessionFactory.getCurrentSession().createQuery("FROM IssuebookEntity where requestBookStatus='Issued'").list();
	
	}

	@Override
	public List<IssuebookEntity> getPendingApprovalBook() {
			
		return this.sessionFactory.getCurrentSession().createQuery("FROM IssuebookEntity where requestBookStatus='pending'").list();
	}

	
	// modify E_issuebook table and E_Booktable
	@Override
	public void updatestatus(String callno, Integer student_Id) {
		
		
		// Update status as "Issued" from "pending" in E_Issuebook table
		final Query query = sessionFactory.getCurrentSession().createQuery("update IssuebookEntity set requestBookStatus='Issued' where callNo=:callno and studentId=:studentID");
		query.setInteger("studentID", student_Id);
		query.setString("callno",callno);
		query.executeUpdate();
	
		// Update count of issued book for the book with callno(bookID)
		final Query query1 = sessionFactory.getCurrentSession().createQuery("update BookEntity set issued= (issued+1) where callNo=:bookid");
		
		query1.setString("bookid",callno);
		query1.executeUpdate();
	
		
		
		
	}

	
}

