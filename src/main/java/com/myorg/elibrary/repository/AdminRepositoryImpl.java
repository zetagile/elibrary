package com.myorg.elibrary.repository;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.myorg.elibrary.entity.LibrarianEntity;


@Repository("adminRepository")

public class AdminRepositoryImpl implements AdminRepository{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void saveLibrarian(LibrarianEntity librarianEntity) {
		
		sessionFactory.getCurrentSession().saveOrUpdate(librarianEntity);
		
	}
		
	@SuppressWarnings("unchecked")
	@Override
	public List<LibrarianEntity> listLibrarians() {
		
		List<LibrarianEntity> LibrarianList = this.sessionFactory.getCurrentSession().createCriteria(LibrarianEntity.class).list();
		return LibrarianList;
	}
	@Override
	public void deleteLibrarian(int id) {
		
		sessionFactory.getCurrentSession().delete(getLibrarianById(id));
		
	}
	@Override
	public LibrarianEntity getLibrarianById(int id) {
	
		return (LibrarianEntity) sessionFactory.getCurrentSession().get(LibrarianEntity.class,id);
	
	}

//	@Override
//	public  LibrarianEntity updateLibrarian(LibrarianEntity librarianEntity) {
//		
//		sessionFactory.getCurrentSession().update(librarianEntity);
//		return librarianEntity;
//	}

	
	
}

