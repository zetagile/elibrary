package com.myorg.elibrary.repository;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.myorg.elibrary.entity.IssuebookEntity;
import com.myorg.elibrary.entity.MemberEntity;



public interface MemberIssueBookRepository {
	// public void addUserBookToCartIssue(Integer callNo,Integer studentId);

	public String addUserBookToCartIssue(MemberEntity memberId, String bookId);
	  
 public  List<IssuebookEntity> getUserBooksInCartIssue(Integer studentId);
 public   List<IssuebookEntity>overDueBooks(Integer studentId);
 public   List<IssuebookEntity> getBookByStudentId(Integer studentId);
  //public void waitlistBooks(Integer callNo, Integer studentId);
void removeCartEntry(String callno,Integer studentId);
public void updaterequestBookStatus(Integer studentId,String callNo);
public List<IssuebookEntity> pendingBooks(Integer studentId);
	
}
