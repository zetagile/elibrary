
package com.myorg.elibrary.repository;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.myorg.elibrary.entity.MemberEntity;
import com.myorg.elibrary.entity.LibrarianEntity;

@Repository
public class LoginRepositoryImpl implements LoginRepository {
	
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public MemberEntity checkMember(String userName,String password)
	{Criterion nm = Restrictions.eq("username",userName);
	Criterion nm1 = Restrictions.eq("password",password);
	List<MemberEntity> members=sessionFactory.getCurrentSession().createCriteria(MemberEntity.class).add(Restrictions.and(nm,nm1)).list();
	
//List<MemberEntity>members= sessionfactory.getCurrentSession().createQuery(" Select username,password from MemberEntity where username='" + member.getUsername() + "' and password='" + member.getPassword()+"'");
   		
    return members.size() > 0 ? members.get(0) : null;
            }

		
	
	  public LibrarianEntity checkLibrarian(String userName,String password)
	  {Criterion nm = Restrictions.eq("username",userName);
		Criterion nm1 = Restrictions.eq("password",password);
		List<LibrarianEntity> Librarian=sessionFactory.getCurrentSession().createCriteria(LibrarianEntity.class).add(Restrictions.and(nm,nm1)).list();
		
	//List<MemberEntity>members= sessionfactory.getCurrentSession().createQuery(" Select username,password from MemberEntity where username='" + member.getUsername() + "' and password='" + member.getPassword()+"'");
	   		
	    return Librarian.size() > 0 ? Librarian.get(0) : null;
	          
		  
	  }

//		@Autowired
//		private SessionFactory sessionFactory;
//
//		@SuppressWarnings("unchecked")
//		@Override
//		public Integer checkUser(LoginEntity user1) {
//			System.out.println("I am in checkuser");
//			String searchprofile = user1.getUserprofile();
//			Criterion nm = Restrictions.eq("username", user1.getUname());
//			Criterion nm1 = Restrictions.eq("password", user1.getPwd());
//			Integer i = 0;
//			if (!(user1.getUname().isEmpty()) && !(user1.getPwd().isEmpty()))
//			{
//				System.out.println("password checking");
//				if (searchprofile.matches("Member")) 
//				{
//					System.out.println("I am a member");
//					MemberEntity memEnt = (MemberEntity) this.sessionFactory.getCurrentSession()
//							.createCriteria(MemberEntity.class).add(Restrictions.and(nm, nm1)).uniqueResult();
//									
//					try
//					{ 
//				           
//						if((memEnt!=null)&&(memEnt.getUsername().matches(user1.getUname()))&&(memEnt.getPassword().matches(user1.getPwd())))
//							{ i = 3; }
//					
//					
//					}catch(NullPointerException e) { 
//		            System.out.print("NullPointerException Caught"); } 
//					
//				}else if(searchprofile.matches("Admin")) {
//				try {
//						
//						System.out.println("I am Admin");
//						if(user1.getUname().equals("admin") && user1.getPwd().equals("admin123"))
//								i=1;
//					}catch(NullPointerException e) { 
//			            System.out.print("NullPointerException Caught");
//			        } 
//					
//				}
//				  
//				else if(searchprofile.matches("Librarian")) {
//				  
//				  /*LibrarianEntity lib1; lib1=
//				  this.sessionFactory.getCurrentSession().createCriteria(LibrarianEntity.class)
//				  .add(Restrictions.and(nm,nm1)).uniqueResult(); i=2;
//				  
//				  */
//					} 			 
//			
//				} 
//				
//				return i;
//			}
//			
}
