
  package com.myorg.elibrary.repository;
  
  import java.util.List;
  
  import org.hibernate.Criteria; import org.hibernate.Query; import
  org.hibernate.SessionFactory; import org.hibernate.criterion.Criterion;
  import org.hibernate.criterion.Order; import
  org.hibernate.criterion.Restrictions; import
  org.springframework.beans.factory.annotation.Autowired; import
  org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.myorg.elibrary.entity.BookEntity;
  
  
  
  @Repository
  public class MemberBookRepositoryImpl implements MemberBookRepository {
  
  @Autowired private SessionFactory sessionFactory;
  
  
  @SuppressWarnings("unchecked")
  @Override
  public List<BookEntity> viewAllBooks() { 
	  return  this.sessionFactory.getCurrentSession().createQuery("FROM BookEntity").list()  ; 
	  }
  
  
  @SuppressWarnings("unchecked")
  @Override
	public List<BookEntity> getBookByName(String sText) {
		String nameSearch= "%"+sText+"%";
		
		return this.sessionFactory.getCurrentSession().createCriteria(BookEntity.class).add(Restrictions.ilike("name",nameSearch)).list();
		}
	
	public List<BookEntity> getBookByAuthor(String sText) {
		String authorSearch= "%"+sText+"%";
		
		return this.sessionFactory.getCurrentSession().createCriteria(BookEntity.class).add(Restrictions.ilike("author",authorSearch)).list();
		}
	

	 
 /* @Override public List<BookEntity> getBookByText(String sText) {
  
  
  // Criterion nm = Restrictions.ilike("name","%sText"); // Criterion nm1 =
  Restrictions.ilike("author","%sText"); // // Criteria //
  qr=sessionFactory.getCurrentSession().createCriteria(BookEntity.class).add(
  Restrictions.and(nm,nm1)); // // qr.se} final Query
  query=sessionFactory.getCurrentSession().
  createQuery("Select * from BookEntity WHERE " +
  "Name like '%book% OR Author like '%book%  ");
  
  query.setString("book", sText);
  
  return query.list();}*/
	 @Override
	 @Transactional
		public BookEntity getBookById(String callNo) {
			// TODO Auto-generated method stub
			final Query query = this.sessionFactory.getCurrentSession().createQuery("FROM BookEntity WHERE callNO=:callNo");
			query.setParameter("callNo", callNo);
			//return(query.executeUpdate());
		return(BookEntity)(query.uniqueResult());
		}
		
  
  }
 