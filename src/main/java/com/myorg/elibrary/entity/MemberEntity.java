package com.myorg.elibrary.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "E_MEMBER")
public class MemberEntity {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer memberID;
	@Column(name = "EMAIL")
	private String email;

//	public String getRole() {
//		return role;
//	}
//
//	public void setRole(String role) {
//		this.role = role;
//	}
//
//	private String role;
	@Column(name = "NAME", nullable = false)
	private String username;
	@Column(name = "PASSWORD", nullable = false)
	private String password;
	@Column(name = "MOBILE", nullable = false)
	private String mobileNO;

	public Integer getMemberID() {
		return memberID;
	}

	public void setMemberID(Integer memberID) {
		this.memberID = memberID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNO() {
		return mobileNO;
	}

	public void setMobileNO(String mobileNO) {
		this.mobileNO = mobileNO;
	}

	public MemberEntity(Integer memberID, String email, String username, String password, String mobileNO) {
		super();
		this.memberID = memberID;
		this.email = email;
		this.username = username;
		this.password = password;
		this.mobileNO = mobileNO;
	}

	public MemberEntity() {
		super();
	}

}
