package com.myorg.elibrary.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "E_BOOK")
public class BookEntity {

	@Id
	@Column(name = "CALLNO", length = 8, unique = true, nullable = false)
	
	//@GeneratedValue
	private String callNo;

	public String getCallNo() {
		return callNo;
	}

	public void setCallNo(String callNo) {
		this.callNo = callNo;
	}

	public Integer getIssued() {
		return issued;
	}

	public void setIssued(Integer issued) {
		this.issued = issued;
	}

	public BookEntity() {
		super();
	}

	@Column(name = "NAME")
	private String name;

	@Column(name = "AUTHOR")
	private String author;

	@Override
	public String toString() {
		return "BookEntity [id=" + callNo + ", name=" + name + ", author=" + author + ", publisher=" + publisher
				+ ", quantity=" + quantity + ", number=" + issued + "]";
	}

	@Column(name = "PUBLISHER")
	private String publisher;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	

	

	@Column(name = "QUANTITY")
	public Integer quantity;
	@Column(name = "ISSUED")
	private Integer issued;

	



}
