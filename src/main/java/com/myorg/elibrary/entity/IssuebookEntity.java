package com.myorg.elibrary.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "E_ISSUEBOOK")
public class IssuebookEntity {
	@Id
	@Column(name = "CALLNO", length = 8, unique = true, nullable = false)
	//@GeneratedValue
	private String callNo;

	@Column(name = "STUDENTID", length = 8, unique = true, nullable = false)
	private Integer studentId;

	@Column(name = "STUDENTNAME")
	private String studentName;
	@Column(name = "STUDENTMOBILE")
	private String studentMobile;

	public String getCallNo() {
		return callNo;
	}

	public void setCallNo(String callNo) {
		this.callNo = callNo;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentMobile() {
		return studentMobile;
	}

	public void setStudentMobile(String studentMobile) {
		this.studentMobile = studentMobile;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public IssuebookEntity() {
		super();
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(String returnStatus) {
		this.returnStatus = returnStatus;
	}

	@Column(name = "ISSUEDDATE")
	private String issueDate;
	public IssuebookEntity(String callNo, Integer studentId, String studentName, String studentMobile,
			 LocalDateTime f, String returnStatus, String requestBookStatus) {
		super();
		this.callNo = callNo;
		this.studentId = studentId;
		this.studentName = studentName;
		this.studentMobile = studentMobile;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	    this.issueDate = dtf.format(f);
		this.returnStatus = returnStatus;
		this.requestBookStatus = requestBookStatus;
	}

	@Column(name = "RETURNSTATUS")
	private String returnStatus;

	
	@Column(name = "requestBookStatus")
	private String requestBookStatus;

	public String getRequestBookStatus() {
		return requestBookStatus;
	}

	public void setRequestBookStatus(String requestBookStatus) {
		this.requestBookStatus = requestBookStatus;
	}
}
