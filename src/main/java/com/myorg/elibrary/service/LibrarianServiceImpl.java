package com.myorg.elibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myorg.elibrary.entity.BookEntity;
import com.myorg.elibrary.entity.IssuebookEntity;
import com.myorg.elibrary.repository.LibrarianRepository;

@Service
public class LibrarianServiceImpl implements LibrarianService {

	@Autowired
	private LibrarianRepository libraryRepo;
	
	@Override
	@Transactional
	public void addbook(BookEntity book) {
		
		
		
		libraryRepo.addbook(book);
		
	}

	
	
	@Override
	@Transactional
	public List<BookEntity> getAllBook() {
		return libraryRepo.getAllBook();
	}



	@Override
	@Transactional
	public List<IssuebookEntity> getAllIssuedBook() {
		
		return libraryRepo.getAllIssuedBook();
	}



	@Override
	@Transactional
	public List<IssuebookEntity> getPendingApprovalBook() {
	
		return libraryRepo.getPendingApprovalBook() ;
	}



	@Override
	@Transactional
	public void updatestatus(String callno, Integer student_Id) {
		
		libraryRepo.updatestatus(callno,student_Id);
	}

	
}