
  package com.myorg.elibrary.service;
  
import com.myorg.elibrary.entity.LibrarianEntity;
import com.myorg.elibrary.entity.MemberEntity;
  
public interface LoginService { 
	  public MemberEntity checkMember(String userName,String password);
	  public LibrarianEntity checkLibrarian(String userName,String password);
}
 