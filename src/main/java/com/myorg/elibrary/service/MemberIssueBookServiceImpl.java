package com.myorg.elibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.myorg.elibrary.entity.IssuebookEntity;
import com.myorg.elibrary.entity.MemberEntity;
import com.myorg.elibrary.repository.MemberIssueBookRepository;


@Service
public class MemberIssueBookServiceImpl implements MemberIssueBookService{
	
	@Autowired
	MemberIssueBookRepository memberIssueBookRep;

	@Override
	@Transactional
	 public String addUserBookToCartIssue(MemberEntity memberId,String bookId)
     {
		// TODO Auto-generated method stub
		 return memberIssueBookRep.addUserBookToCartIssue(memberId, bookId);
	}

//	@Override
//	@Transactional
//	public List<IssuebookEntity> viewIssueBooksInCart() {
//		// TODO Auto-generated method stub
//		return  memberIssueBookRep.viewIssueBooksInCart();
//	}
	
	
	@Override
	@Transactional
	public void removeCartEntry(String callNo,Integer studentId) {
		// TODO Auto-generated method stub
		memberIssueBookRep.removeCartEntry(callNo,studentId);
	}

	@Override
	@Transactional
	public List<IssuebookEntity> getUserBooksInCartIssue(Integer studentId) {
		// TODO Auto-generated method stub
		return memberIssueBookRep.getUserBooksInCartIssue(studentId);
	}

	@Override
	@Transactional
	public List<IssuebookEntity> overDueBooks(Integer studentId) {
		// TODO Auto-generated method stub
		return memberIssueBookRep.overDueBooks( studentId);
	}

	@Override
	@Transactional
	public List<IssuebookEntity> getBookByStudentId(Integer studentId) {
		// TODO Auto-generated method stub
		return memberIssueBookRep.getBookByStudentId(studentId);
	}
	@Override
	@Transactional
	public void updaterequestBookStatus(Integer studentId,String callNo)
	{
		 memberIssueBookRep.updaterequestBookStatus(studentId,callNo);
	}

//	@Override
//	@Transactional
//	public void waitlistBooks(Integer callNo, Integer studentId) {
//		// TODO Auto-generated method stub
//		memberIssueBookRep.waitlistBooks(callNo, studentId);
//	}
	@Override
	@Transactional
	
	public List<IssuebookEntity> pendingBooks(Integer studentId)
	{return memberIssueBookRep.pendingBooks(studentId);
}
}