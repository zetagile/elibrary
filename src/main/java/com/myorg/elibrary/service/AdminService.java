package com.myorg.elibrary.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.myorg.elibrary.entity.LibrarianEntity;


public interface AdminService {

	public void saveLibrarian(LibrarianEntity librarianEntity);
//	public LibrarianEntity updateLibrarian(LibrarianEntity librarianEntity);
	public List<LibrarianEntity> listLibrarians();
	public void deleteLibrarian(int id);
	public LibrarianEntity getLibrarianById(int id);
	
	
}
	