
  package com.myorg.elibrary.service;
  
  import java.util.List;
  
  import com.myorg.elibrary.entity.BookEntity;
  
  
  
  public interface MemberBookService { 
	  public List<BookEntity> viewAllBooks();
	  public List<BookEntity> getBookByName(String sText);
	  public List<BookEntity> getBookByAuthor(String sText);
	  //public void requestBook(String callno);
	  public BookEntity getBookById(String callNo);
}
 