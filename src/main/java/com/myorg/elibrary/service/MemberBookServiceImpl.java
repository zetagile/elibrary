
  package com.myorg.elibrary.service;
  
  import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myorg.elibrary.entity.BookEntity;
import  com.myorg.elibrary.repository.MemberBookRepository;
  
  
  
  @Service 
  public class MemberBookServiceImpl implements MemberBookService {
  
  @Autowired private MemberBookRepository memberBookRepo;
  
  @Override
  @Transactional 
  public List<BookEntity> viewAllBooks(){
	  return  memberBookRepo.viewAllBooks(); }
  
    @Override
	@Transactional
	public List<BookEntity> getBookByName(String sText){
		return memberBookRepo.getBookByName(sText);
	}
	
	@Override
	@Transactional
	public List<BookEntity> getBookByAuthor(String sText){
		return memberBookRepo.getBookByAuthor(sText);
	}
	
//	@Override
//	@Transactional
//	public void requestBook(String callno) {
//		memberBookRepo.requestBook(callno);
//		
//	}

	
	@Transactional
	public BookEntity getBookById(String callNo) {
		// TODO Auto-generated method stub
		return memberBookRepo.getBookById(callNo);
	}
	
  //@Override
  //@Transactional
  //public List<BookEntity> getBookByText(String sText){ return
  //memberBookRepo.getBookByText(sText); }
	
  
  
  }