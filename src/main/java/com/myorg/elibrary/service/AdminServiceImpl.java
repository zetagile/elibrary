	package com.myorg.elibrary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myorg.elibrary.entity.LibrarianEntity;
import com.myorg.elibrary.repository.AdminRepository;

@Service("adminService")
@Transactional
public class AdminServiceImpl implements AdminService  {
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Override
	@Transactional
	public List<LibrarianEntity> listLibrarians() {
		
		return adminRepository.listLibrarians();
	}
	@Override
	@Transactional
	public void saveLibrarian(LibrarianEntity librarianEntity) {
	
		adminRepository.saveLibrarian(librarianEntity);
	}
	@Override
	@Transactional
	public void deleteLibrarian(int id) {
		
		adminRepository.deleteLibrarian(id);
		
	}

	@Override
	@Transactional
	public LibrarianEntity getLibrarianById(int id) {
		
		return adminRepository.getLibrarianById(id);
	}
	
//	@Override
//	public LibrarianEntity updateLibrarian(LibrarianEntity librarianEntity) {
//		
//		return adminRepository.updateLibrarian(librarianEntity);
//	}

	
}