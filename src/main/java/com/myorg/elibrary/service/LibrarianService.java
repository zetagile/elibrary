package com.myorg.elibrary.service;

import java.util.List;

import com.myorg.elibrary.entity.BookEntity;
import com.myorg.elibrary.entity.IssuebookEntity;

public interface LibrarianService {

		   
	public void addbook(BookEntity book);

	public List<BookEntity> getAllBook();

	public List<IssuebookEntity> getAllIssuedBook();

	public List<IssuebookEntity> getPendingApprovalBook();

	public void updatestatus(String callno, Integer student_Id);
	
}
