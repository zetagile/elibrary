
  package com.myorg.elibrary.service;
  
  import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myorg.elibrary.entity.LibrarianEntity;
import com.myorg.elibrary.entity.MemberEntity;
import com.myorg.elibrary.repository.LoginRepository;
  
  
  @Service 
  public class LoginServiceImp implements LoginService {
  
	  @Autowired private LoginRepository userRepo;
	  
	  @Override
	  @Transactional 
	  public MemberEntity checkMember(String userName,String password) {
		  return userRepo.checkMember(userName, password);
	  }
	  @Override
	  @Transactional
	  public LibrarianEntity checkLibrarian(String userName,String password) {
		  return userRepo.checkLibrarian(userName, password);
	  }
  }
 