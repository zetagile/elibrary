<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>
<body>

	<h2>Library Management Screen</h2>
<a href="/elibrary/HomeLib">home</a>
	<form:form method="post" action="addbookdetails" commandName="book">

		<table>
			<tr>
				<td><form:label path="callNo">
						<spring:message code="label.callno" />
					</form:label></td>
				<td><form:input path="callNo" /></td>
			</tr>
			<tr>
				<td><form:label path="name">
						<spring:message code="label.name" />
					</form:label></td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td><form:label path="author">
						<spring:message code="label.author" />
					</form:label></td>
				<td><form:input path="author" /></td>
			</tr>
			<tr>
				<td><form:label path="publisher">
						<spring:message code="label.publisher" />
					</form:label></td>
				<td><form:input path="publisher" /></td>
			</tr>
			
			<tr>
				<td><form:label path="quantity">
						<spring:message code="label.quantity" />
					</form:label></td>
				<td><form:input path="quantity" /></td>
			</tr>
		<tr>
				<td><form:label path="issued">
						<spring:message code="label.issued" />
					</form:label></td>
				<td><form:input path="issued" /></td>
			</tr>
				
			<tr>
				<td colspan="2"><input type="submit"
					value="<spring:message code="label.addbook"/>" /></td>
				
			</tr>
		
		</table>
	</form:form>


	
</body>
</html>