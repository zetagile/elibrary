<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>
<body>
 <a href="/elibrary/HomeLib">home</a>
	<h2>Available Books</h2>
	<h3></h3>
	<c:if test="${!empty bookList}">
		<table border="1" class="data">
			<tr>
				<th>CallNo</th>
				<th>Name</th>
				<th>Author       </th>
				<th>Publisher</th>
				<th>quantity</th>
				<th>Books Issued</th>
				
			</tr>
			
			<c:forEach items="${bookList}" var="book">
				<tr>
					<td>${book.callNo}</td>
					<td>${book.name}</td>
					<td>${book.author}       </td>
					
					<c:if test="${!empty book.publisher}">
					<td>${book.publisher}</td>
					</c:if>
					<c:if test="${empty book.publisher}">
					<td> </td>
					</c:if>
					
					
					<c:if test="${!empty book.quantity}">
					<td>${book.quantity}</td>
					</c:if>
					<c:if test="${empty book.quantity}">
					<td>0</td>
					</c:if>
					
					<c:if test="${!empty book.issued}">
					<td>${book.issued}</td>
					</c:if>
					<c:if test="${empty book.issued}">
					<td>0</td>
					</c:if>
					
				
				</tr>
			</c:forEach>
		</table>
	</c:if>
	

	


	
</body>
</html>