<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html lang="en">
  <head>
  <!-- Bootstrap CSS -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
	<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
  <a class="navbar-brand" href="#">eLibrary</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Home</a>
      </li>
      <li class="nav-item ">
      	 <a class="nav-link" name= "view" href="#">View Librarian<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ">
         <a class="nav-link" href="#">Add Librarian</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Add Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">View Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Issue Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">View Issued Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Return Book</a>
      </li>
      
      
       <li class="nav-item active">
        <a class="nav-link" href="viewbooks">View Book</a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="viewbooksByAuthor">Search by Author</a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="viewbooksByName">Search by B/Name</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="issueBooks">My cart</a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="pendingBooks">My Requests</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="overDue">My Overdues</a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="issuedBooks">My All Books</a>
      </li>
      
      <li class="nav-item active">
        <a class="nav-link" href="logout">Logout</a>
      </li>
    </ul>
    <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button type="button" class="btn btn-primary">Search</button>
  </form>
  </div>
</nav>
<title>Welcome to the Member Page</title>
</head>
<body>
<div class="container text-center" id="tasksDiv">
<br>
	<c:if test="${!empty bookList}">
				<h3><p style="font-size:30px;color:blue;">View Books</p></h3>
				<br>
				<div class="table-responsive">
					<table class="table table-striped table-bordered text-left">
						<thead>
							<tr>
							<th>Name</th>
							<th>Author</th>
							<th>Publisher</th>
							<th>Quantity</th>
							<th>Issued</th>
							<th>Activity</th>
						</tr>
						</thead>
						<tbody>
						<!-- loop over and print our books -->
						<c:forEach var="mem" items="${bookList}">

							<tr>
								<td>${mem.name}</td>
								<td>${mem.author}</td>
								<td>${mem.publisher}</td>
								<td>${mem.quantity}</td>
								<td>${mem.issued}</td>	
								<td><a href = "requestBook/${mem.callNo}"> Request</a></td>
								
							</tr>	

						</c:forEach>
						</tbody>
					</table>
				</div>
			
</c:if>
<br><br>
	<!-- <br><br><INPUT TYPE="button" VALUE="Back" onClick="history.go(-1);"> -->

<form:form method="get" action="searchingAuthor" commandName="e_book">
	<Table>
			
			<tr>
				<td>Enter Author </td>
				<td><form:label path="author">
						<spring:message code="label.author" />
					</form:label></td>
				<td><form:input path="author" /></td>
				
				
				<td colspan="2"><input type="submit"
					value="Search" /></td>
					
		</tr>
		</table>
	</form:form>


</div>


 </body>
</html>
   