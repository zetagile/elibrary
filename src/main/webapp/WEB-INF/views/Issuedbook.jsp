<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>
<body>
 <a href="/elibrary/HomeLib">home</a>
	<h2>Issued Books</h2>
	<h3></h3>
	<c:if test="${!empty issuedBookList}">
		<table class="data" border= 1 bgcolor=  skyblue>
			<tr>
				<th>CallNo</th>
				<th>StudentId</th>
				<th>StudentName      </th>
				<th>StudentMobile</th>
				<th>IssuedDate</th>
				<th>ReturnStatus</th>
				<th>RequestBookStatus</th>
			</tr>
			
			<c:forEach items="${issuedBookList}" var="issuebook">
				<tr>
					<td>${issuebook.callNo}</td>
					<td>${issuebook.studentId}</td>
					<td>${issuebook.studentName}</td>
					<td>${issuebook.studentMobile}</td>
					<td>${issuebook.issueDate}</td>
					<td>${issuebook.returnStatus}</td>
					<td>${issuebook.requestBookStatus}</td>
		
					
				
				</tr>
			</c:forEach>
		</table>
	</c:if>
	

	


	
</body>
</html>