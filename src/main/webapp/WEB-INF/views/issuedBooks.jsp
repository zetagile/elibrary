<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html lang="en">
  <head>
  <!-- Bootstrap CSS -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
	<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
  <a class="navbar-brand" href="#">eLibrary</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Home</a>
      </li>
      <li class="nav-item ">
      	 <a class="nav-link" name= "view" href="#">View Librarian<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ">
         <a class="nav-link" href="#">Add Librarian</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Add Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">View Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Issue Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">View Issued Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Return Book</a>
      </li>
      
      
       <li class="nav-item active">
        <a class="nav-link" href="viewbooks">View Book</a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="viewbooksByAuthor">Search by Author</a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="viewbooksByName">Search by B/Name</a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="issueBooks">My cart</a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="pendingBooks">My Requests</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="overDue">My Overdues</a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="issuedBooks">My All Books</a>
      </li>
      
      <li class="nav-item active">
        <a class="nav-link" href="logout">Logout</a>
      </li>
    </ul>
    <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button type="button" class="btn btn-primary">Search</button>
  </form>
  </div>
</nav>
<title>Welcome to the Member Page</title>
</head>
<body>

	
 <h3>The Student ${ bookStatus }s</h3>

	<c:if test="${!empty issuedBooks}">
		<table class="data" border= 1 bgcolor=  skyblue>
			<tr>
				<th>callNo</th>
				<th>studentId</th>
				<th>studentName</th>
				<th>studentMobile</th>
				<th>issueDate</th>
				<th>returnStatus</th>
				<th>requestBookStatus</th>
				<th>&nbsp;</th>
			</tr>
			<c:forEach items="${issuedBooks}" var="isb">
				<tr>
					
					<td>${isb.callNo}</td>
					<td>${isb.studentId}</td>
						<td>${isb.studentName}</td>
							<td>${isb.studentMobile}</td>
							<td>${isb.issueDate}</td>
							<td>${isb.returnStatus}</td>
							<td>${isb.requestBookStatus}</td>
					
				</tr>
			</c:forEach>
		</table>
	</c:if>
 <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>