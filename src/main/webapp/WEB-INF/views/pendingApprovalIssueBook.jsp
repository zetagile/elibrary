<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>
<body>
 <a href="/elibrary/HomeLib">home</a>
	<h2>Pending Approval from Librarian</h2>
	<h3></h3>
	<c:if test="${!empty issuedBookList1}">
		<table border="1" class="data">
			<tr>
				<th>CallNo</th>
				<th>StudentId</th>
				<th>StudentName      </th>
				<th>StudentMobile</th>
				<th>IssuedDate</th>
				<th>ReturnStatus</th>
				<th>RequestStatus</th>
				<th></th>
			</tr>
			
			<c:forEach items="${issuedBookList1}" var="issuebook1">
				<tr>
					<td>${issuebook1.callNo}</td>
					<td>${issuebook1.studentId}</td>
					<td>${issuebook1.studentName}</td>
					<td>${issuebook1.studentMobile}</td>
					<td>${issuebook1.issueDate}</td>
					<td>${issuebook1.returnStatus}</td>
					<td>${issuebook1.requestBookStatus}</td>
					<td><a href=approve/${issuebook1.callNo}/${issuebook1.studentId}/>Approve</td>
					
					
				
				</tr>
			</c:forEach>
		</table>
	</c:if>
	

	


	
</body>
</html>