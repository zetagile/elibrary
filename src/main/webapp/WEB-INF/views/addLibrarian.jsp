<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html lang="en">
  <head>
     <!-- Bootstrap CSS -->
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=2">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="resources/css/addlib.css" rel="stylesheet" type="text/css">
	<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
  <a class="navbar-brand" href="#">eLibrary</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Home</a>
      </li>
      <li class="nav-item active">
      	 <a class="nav-link" name= "view" href="/elibrary/list">View Librarian<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="/elibrary/showForm">Add Librarian</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Add Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">View Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Issue Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">View Issued Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Return Book</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">Logout</a>
      </li>
    </ul>
    <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button type="button" class="btn btn-primary">Search</button>
  </form>
  </div>
</nav>

   <title>Manage Librarian</title>
  </head>
  <body>
  <script>
 
	  //document.getElementById("addForm").submit(); 
	function myFunction()
  {
		document.getElementById("addForm").submit();
		document.getElementById("validationDefaultUsername").value = "";
	    document.getElementById("inputPassword4").value = "";
	    document.getElementById("inputEmail4").value = "";
	    document.getElementById("Contact").value = "";
  }
	function myFunction1()
	  {
			document.getElementById("addForm").reset();
			
	  }
</script>
 <br><br><br>
 <br>
 <div class="container register-form">
 <form:form action="/elibrary/saveLibrarian" method="post" modelAttribute="librarianEntity" id="addForm" name="addForm" >
     <!-- need to associate this data with librarian id -->
  
		<form:hidden path="id"/>  
            <div class="form">
                <div class="note">
                    <p>ADD/EDIT Librarian</p>
                </div>

                <div class="form-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <div class="input-group">
       						 <div class="input-group-prepend">
          						<span class="input-group-text" id="inputGroupPrepend2">@</span>
       						 </div>
        					<input type="text" path="username" name ="username" class="form-control" id="validationDefaultUsername" aria-describedby="inputGroupPrepend2" placeholder="Username" value ="${librarianEntity.username}" required/>
        					<form:errors path="username"></form:errors>
      						</div>
                            </div>
                            <div class="form-group">
                             <input type="password" path="password" name="password" class="form-control" id="inputPassword4" placeholder="Password" value ="${librarianEntity.password}" required />
                             <form:errors path="password"></form:errors>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" path="email" name="email" class="form-control" id="inputEmail4" value ="${librarianEntity.email}" placeholder="Email" required />
                                <form:errors path="email" ></form:errors>
                            </div>
                            <div class="form-group">
                                <input type="text" path="mobile" name="mobile" class="form-control" id="Contact" placeholder="mobile"value ="${librarianEntity.mobile}" required/>
                                <form:errors path="mobile"></form:errors>
                            </div>
                        </div>
                       
                    </div>
                    <center> <input type="button" id=submitform class="btnSubmit" value="Submit" onclick="myFunction();"/><button id=resetform class="btnSubmit"><a href="/elibrary/list" style="color:inherit">Cancel</a></button> </center>
                    <br><center><button type="reset" class="btn btn-light" onclick="myFunction1();">Reset</button>
                    
                </div>
           		 </div>
       			 </div>
       		  </div>
       		
  		</form:form>
  
  </body>
</html>
 