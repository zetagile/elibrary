<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="resources/style1.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/reasle/v5.0.7/js/all.js"></script>
     <nav class="navbar navbar-dark bg-primary">
  <span class="navbar-text">
    eLibrary
  </span>
</nav>
</head>
<body>
	<body>
        <div class="modal-dialog text-center">
            <div  class="main-section">
           <div class="modal-content">
               
               <div class="col-12 user-img">
                  <img src="resources/face.png" alt="..." class="img-rounded">
               </div>
                 <!-- Palak added action ="login" and method ="post" added name property to all the controls-->
               <form class="col-12" action="/elibrary/login" method="post">
                   <div class="form-group">
                   	<div class="form-group col-md-4">
      <select name="userprofile"   id="inputProfile" class="form-control">
        <option selected>Member</option>
        <option>Librarian</option>  
        <option>Admin</option>
      </select>
    </div>
                       <input name = "uname" type="text" class="form-control" placeholder="User Name">
                   </div>
                   <div class="form-group">
                       <input name = "pwd" type="password" class="form-control" placeholder="Password">
                   </div>
                   <button type="submit" class="btn"><i class="fas fa-sign-in-alt"></i>Login</button>
                   
                   <div style="color: red">${error}</div>
               </form>
               <div class="col-12 forgot">
                   <a href="#">Forgot Password?</a>
               </div>
                </div> <!---End of Modal Content-->
                </div>
 </form>
                <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html> 

<%-- =======
</body>
=======
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="style1.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/reasle/v5.0.7/js/all.js"></script>
     <nav class="navbar navbar-dark bg-primary">
  <span class="navbar-text">
    eLibrary
  </span>
</nav>
</head>
<body>
	<body>
        <div class="modal-dialog text-center">
            <div  class="main-section">
           <div class="modal-content">
               
               <div class="col-12 user-img">
                   <img src="face.png">
               </div>
               
               
               <!-- Palak added action ="login" and method ="post" added name property to all the controls-->
               <form class="col-12" action="login" method="post">
                   <div class="form-group">
                   	<div class="form-group col-md-4">
      <select name="userprofile"   id="inputProfile" class="form-control">
        <option selected>Member</option>
        <option>Librarian</option>  
        <option>Admin</option>
      </select>
    </div>
                       <input name = "uname" type="text" class="form-control" placeholder="User Name">
                   </div>
                   <div class="form-group">
                       <input name = "pwd" type="password" class="form-control" placeholder="Password">
                   </div>
                   <button type="submit" class="btn"><i class="fas fa-sign-in-alt"></i>Login</button>
               </form>
               <div class="col-12 forgot">
                   <a href="#">Forgot Password?</a>
               </div>
                </div> <!---End of Modal Content-->
                </div>

                <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
<%-- =======
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="style1.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/reasle/v5.0.7/js/all.js"></script>
     <nav class="navbar navbar-dark bg-primary">
  <span class="navbar-text">
    eLibrary
  </span>
</nav>
</head>
<body>
	<body>
        <div class="modal-dialog text-center">
            <div  class="main-section">
           <div class="modal-content">
               
               <div class="col-12 user-img">
                   <img src="face.png">
               </div>
               <form class="col-12">
                   <div class="form-group">
                   	<div class="form-group col-md-4">
      <select id="inputProfile" class="form-control">
        <option selected>Member</option>
        <option>Librarian</option>
        <option>Admin</option>
      </select>
    </div>
                       <input type="text" class="form-control" placeholder="User Name">
                   </div>
                   <div class="form-group">
                       <input type="password" class="form-control" placeholder="Password">
                   </div>
                   <button type="submit" class="btn"><i class="fas fa-sign-in-alt"></i>Login</button>
               </form>
               <div class="col-12 forgot">
                   <a href="#">Forgot Password?</a>
               </div>
                </div> <!---End of Modal Content-->
                </div>

                <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
>>>>>>> branch 'master' of https://SanthiAmbati@bitbucket.org/zetagile/elibrary.git 

>>>>>>> branch 'master' of https://Malinimr@bitbucket.org/zetagile/elibrary.git 
</html> --%>