<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html lang="en">
  <head>
  <!-- Bootstrap CSS -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
	<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
  <a class="navbar-brand" href="#">eLibrary</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Home</a>
      </li>
      <li class="nav-item active">
      	 <a class="nav-link" name= "view" href="/elibrary/list">View Librarian<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
         <a class="nav-link" href="/elibrary/showForm">Add Librarian</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Add Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">View Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Issue Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">View Issued Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Return Book</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Logout</a>
      </li>
    </ul>
    <form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button type="button" class="btn btn-primary">Search</button>
  </form>
  </div>
</nav>
 <title>View Librarian</title>
<body>
<div class="container text-center" id="tasksDiv">
<br>
				<h3><p style="font-size:30px;color:blue;">View Librarian</p></h3>
				<br>
				<div class="table-responsive">
					<table class="table table-striped table-bordered text-left">
						<thead>
							<tr>
							<th>Librarian ID</th>
							<th>Username</th>
							<th>Password</th>
							<th>Email</th>
							<th>Mobile</th>
							<th>Edit</th>
							<th>Remove</th>
						</tr>
						</thead>
						<tbody>
						<!-- loop over and print our librarians -->
						<c:forEach var="tempLibrarian" items="${librarianEntity}">

							<!-- construct an "update" link with librarian id -->
							<c:url var="updateLink" value="/editLibrarian">
								<c:param name="id" value="${tempLibrarian.id}" />
							</c:url>

							<!-- construct an "delete" link with librarian id -->
							<c:url var="deleteLink" value="/deleteLibrarian">
								<c:param name="id" value="${tempLibrarian.id}" />
							</c:url>

							<tr>
								<td>${tempLibrarian.id}</td>
								<td>${tempLibrarian.username}</td>
								<td>${tempLibrarian.password}</td>
								<td>${tempLibrarian.email}</td>
								<td>${tempLibrarian.mobile}</td>
								
									<!-- display the update link --> 
									
								<td>	<a href="${updateLink}"><span class="glyphicon glyphicon-pencil"></span></a></td>
								<td>	<a href="${deleteLink}"
									onclick="if (!(confirm('Are you sure you want to delete this librarian?'))) return false"><span class="glyphicon glyphicon-remove"></span></a>
								</td>

							</tr>	

						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			  


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>