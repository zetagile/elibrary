<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<body>

	

	
<h3>SubmittedBooks</h3>

	<c:if test="${!empty issuedBooks}">
		<table class="data">
			<tr>
				<th>callNo</th>
				<th>studentId</th>
				<th>studentName</th>
				<th>studentMobile</th>
				<th>issueDate</th>
				<th>returnStatus</th>
				<th>requestBookStatus</th>
				<th>&nbsp;</th>
			</tr>
			<c:forEach items="${issuedBooks}" var="isb">
				<tr>
					
					<td>${isb.callNo}</td>
					<td>${isb.studentId}</td>
						<td>${isb.studentName}</td>
							<td>${isb.studentMobile}</td>
							<td>${isb.issueDate}</td>
							<td>${isb.returnStatus}</td>
							<td>${isb.requestBookStatus}</td>
					
				</tr>
			</c:forEach>
		</table>
	</c:if>

</body>
</html>